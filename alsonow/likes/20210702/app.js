let hash;
let href;
let pathname;
let search;
let config = {}

document.addEventListener('DOMContentLoaded',load);
let root = document.getElementById('root')

function load(){
  console.log('load page...');
  localStorage_handle_data();
  parseUrl();
  viewPage(config.page.name);
  bulma_handle_burger();
  document.querySelectorAll('[data-button]').forEach(item => {
    console.log(item)
    item.addEventListener('click',data_button_clicked);
  })
}
function parseUrl(){
  hash = location.hash;
  href = location.href;
  pathname = location.pathname;
  search = location.search;
  hash = parseUrl_handle_hash(location.hash);
  search = parseUrl_handle_search(location.hash,location.search);
  window.addEventListener('popstate',load);
  parseUrl_handle_data(hash,search);
  if (config.search.fetch === "true"){
    fetchGithubJsonFile({
        "token":config.page.token,
        "username":config.page.user,
        "reponame":config.page.repo,
        "filepath":config.page.file
      }
    );
  }
}
async function fetchGithubJsonFile(parameters){
  let username = parameters.username;
  let reponame = parameters.reponame;
  let filepath = parameters.filepath;
  let token = parameters.token;
  let url = `https://api.github.com/repos/${username}/${reponame}/contents/${filepath}`;
  const headers = {
    "Authorization" : `Token ${token}`
  }
  const response = await fetch(url,{
    method : "GET",
    headers : headers
  })
  const result = await response.json()
  const result_json = JSON.parse(atob(result.content));
  fetchGithubJsonFile_handle_data(result_json);
}
function parseUrl_handle_hash(hash){
  if (hash === '') return;
  hash = hash.split('#/')[1].split('?')[0].split('/');
  return hash;
}
function parseUrl_handle_search(hash,search){
  if (hash === '' && search === '') return;
  let searchList = [];
  if (search === ''){
    if (hash.split('#/')[1] === undefined || hash.split('#/')[1].split('?')[1] === undefined) return;
    hash.split('#/')[1].split('?')[1].split('&').forEach(item => {
      searchList.push(item.split('='));
    });
  } else {
    search.split('?')[1].split('&').forEach(item => {
      searchList.push(item.split('='));
    });
  }
  return searchList;
}
function parseUrl_handle_conf(confpath){
  confpath = confpath.split('/');
  config['page']['user'] = confpath[0];
  config['page']['repo'] = confpath[1];
  config['page']['file'] = confpath[2];
}
function fetchGithubJsonFile_handle_data(data){
  config['configFile'] = data;
  localStorage.setItem('config_file',btoa(data.stringify))
}
function parseUrl_handle_data(hash,search){
  if (config.page === undefined) config.page = {};
  if (hash === undefined) location.hash = '#/view/';
  let page = hash[0];
  let word;
  switch (page) {
    case 'view':
      config['page']['name'] = 'view';
      word = hash[1] === '' ?undefined :hash[1];
      config['page']['word'] = word
      break;
    case 'send':
      config['page']['name'] = 'send';
      word = hash[1] === '' ?undefined :hash[1];
      config['page']['word'] = word
      break;
    case 'settings':
      config['page']['name'] = 'settings';
      break;
    default:
      break;
  }
  config.search = {};
  if (search !== undefined){
    for (let i = 0; i < search.length; i++) {
      let key = search[i][0];
      let value = search[i][1];
      if (key !== 'id') config['search'][key] = value;
    }
  }
}
function viewPage(page){
  root.innerHTML = '';
  let template = '';
  let nav = '';
  let footer = '';
  document.querySelectorAll('[data-page]').forEach(i => {
    if (i.dataset.page === page){
      template = i.content.cloneNode(true);
    }
  });
  document.querySelectorAll('[data-role]').forEach(i => {
    if (i.dataset.role === 'nav'){
      nav = i.content.cloneNode(true);
    } else if (i.dataset.role === 'footer'){
      footer = i.content.cloneNode(true);
    }
  });
  if (template === ''){
    template = document.getElementById('404').innerHTML;
  }
  root.appendChild(nav);
  root.appendChild(template);
  root.appendChild(footer);
  viewPage_handle_a(root);
  viewPage_handle_id(config.search.id);
}
function viewPage_handle_a(element){
  for (let i = 0; i < element.children.length; i++) {
    if (element.children[i].tagName === 'A') {
      if (element.children[i].dataset.link !== undefined){
        element.children[i].classList.add('link');
        element.children[i].addEventListener('click',navClickableItem);
      }
    }
    if (element.children[i].children !== undefined) viewPage_handle_a(element.children[i]);
  }
}
function navClickableItem(e){
  goHash(e.target.dataset.link,e.target.dataset.linkid);
}
function goHash(gohash,goid){
  let before = href.split(pathname)[0]
  let newsearch = '';
  if (goid !== undefined) newsearch = '?';
  if (goid !== undefined) newsearch += `id=${goid}`;
  if (JSON.stringify(config.search) !== "{}" && goid !== undefined) newsearch += '&';
  if (search !== undefined){
    for (let i = 0; i < search.length; i++) {
      switch (search[i][0]) {
        case 'id':
          
          break;
      
        default:
          if (newsearch === '') newsearch = '?';
          newsearch += `${search[i][0]}=${search[i][1]}`
          if (i !== search.length - 1) newsearch += '&'
          break;
      }
    }
  }
  if (config.page.word !== undefined && gohash !== '#/settings/') gohash = gohash + config.page.word;
  let newhref = before+pathname+gohash+newsearch;
  location.href = newhref;
}
function viewPage_handle_id(id){
  if (id === undefined) return;
  document.getElementById(id).scrollIntoView();
}
function bulma_handle_burger(){
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach( el => {
      el.addEventListener('click', () => {

        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }
}
function data_button_clicked(e){
  let button = e.target.dataset.button;
  switch (button) {
    case 'submit-token':
      let token = document.getElementById('settings-token').value
      let filepath = document.getElementById('settings-filepath').value
      localStorage.setItem("github_token", btoa(token));
      localStorage.setItem("github_filepath", btoa(filepath));
      console.log('saved',token,filepath)
      break;
  
    default:
      break;
  }
}
function localStorage_handle_data(){
  if (config.page === undefined) config.page = {};
  let token = atob(localStorage.getItem('github_token'))
  let filepath = atob(localStorage.getItem('github_filepath'))
  let configfile = atob(localStorage.getItem('config_file')).JSON.parse
  parseUrl_handle_conf(filepath)
  config['page']['token'] = token
  config['configFile'] = configfile
}
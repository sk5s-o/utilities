async function getRepos(url){
  url = "https://api.github.com/search/repositories?q=stars:150000..350000"
  const response = await fetch(url)
  const result = await response.json()
  console.log(result);
  let maindiv = document.getElementById('main');
  maindiv.innerHTML = '';
  result.items.forEach(i => {
    const anchor = document.createElement('a')
    anchor.href = i.html_url;
    anchor.target = '_blank';
    anchor.textContent = i.full_name;
    maindiv.appendChild(anchor);
    maindiv.appendChild(document.createElement('br'));
  });
}
async function getIssues(url){
  url = "https://api.github.com/search/issues?q=author:lasjorg repo:freeCodeCamp/freeCodeCamp type:issue";
  const response = await fetch(url)
  const result = await response.json()
  console.log(result);
  let maindiv = document.getElementById('main');
  maindiv.innerHTML = '';
  result.items.forEach(i => {
    const anchor = document.createElement('a')
    anchor.href = i.html_url;
    anchor.target = '_blank';
    anchor.textContent = i.title;
    maindiv.appendChild(anchor);
    maindiv.appendChild(document.createElement('br'));
  });
}
async function getCommits(url="https://api.github.com/search/commits?q=author-date:2021-01-01..2021-06-29 repo:freeCodeCamp/freeCodeCamp"){
  const response = await fetch(url,{
    "method" : "GET",
    "headers" : {
      "Accept" : "application/vnd.github.cloak-preview"
    }
  });
  const link = response.headers.get('link')
  const links = link.split(',')
  const urls = links.map(i => {
    return {
      url: i.split(";")[0].replace(">","").replace("<",""),
      title: i.split(";")[1]
    }
  })
  const result = await response.json()
  console.log(result);
  let maindiv = document.getElementById('main');
  maindiv.innerHTML = '';
  result.items.forEach(i => {
    const img = document.createElement('img');
    const anchor = document.createElement('a')
    anchor.href = i.html_url;
    anchor.target = '_blank';
    anchor.textContent = i.commit.message.substr(0,100) + "...";
    img.src = i.author.avatar_url;
    img.width = 50;
    maindiv.appendChild(img);
    maindiv.appendChild(anchor);
    maindiv.appendChild(document.createElement('hr'));
  });
  urls.forEach(i => {
    const btn = document.createElement('button')
    btn.textContent = i.title
    btn.addEventListener('click',e => getCommits(i.url))
    maindiv.appendChild(btn)
  })
}
async function getPrivateIssues(url){
  url = "https://api.github.com/search/issues?q=repo:samko5sam/g-form-likes-private type:issue";
  const headers = {
    "Authorization" : `Token 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`
  }
  const response = await fetch(url,{
    "method" : "GET",
    "headers" : headers
  })
  const result = await response.json()
  console.log(result);
  let maindiv = document.getElementById('main');
  maindiv.innerHTML = '';
  result.items.forEach(i => {
    const anchor = document.createElement('a')
    anchor.href = i.html_url;
    anchor.target = '_blank';
    anchor.textContent = i.title;
    maindiv.appendChild(anchor);
    maindiv.appendChild(document.createElement('br'));
  });
}
async function createIssue(issuetitle="create issue!"){
  let url = "https://api.github.com/repos/samko5sam/g-form-likes-private/issues";
  const headers = {
    "Authorization" : `Token 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`
  }
  const payLoad = {
    title: issuetitle
  }
  const response = await fetch(url,{
    method : "POST",
    headers : headers,
    body : JSON.stringify(payLoad)
  })
}
async function getContent(){
  let url = "https://api.github.com/repos/samko5sam/g-form-likes-private/contents/config.json";
  const headers = {
    "Authorization" : `Token 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`
  }
  const response = await fetch(url,{
    method : "GET",
    headers : headers
  })
  const result = await response.json()
  const result_json = JSON.parse(atob(result.content));
  console.log(result_json);
}
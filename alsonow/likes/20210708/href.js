"use strict";
// https://dev.to/aminnairi/a-router-without-a-web-server-in-vanilla-javascript-3bmg
class Href{
  constructor(myobject){
    this.handle_constructor_object(myobject);
    this.searchParams = [];
  }
  handle_constructor_object(myobject){
    console.log('handle Href constructor ...')
    this.defaultHash = myobject.defaultHash === undefined ? '#/home/' : myobject.defaultHash;
    this.viewId = myobject.viewId;
    this.viewContent = myobject.viewContent;
    this.showNum = myobject.showNum === null ? 10 : myobject.showNum;
  }
  routePage() {
    const hash = window.location.hash;
    const routerView = document.getElementById("router-view");
    if (!(routerView instanceof HTMLElement)) {
      throw new ReferenceError("No router view element available for rendering");
    }
    switch (hash) {
      // general page
      case '#/view/': case '#/view':
        routerView.innerHTML = document.getElementById('page_part_tab').innerHTML;
        routerView.innerHTML += document.getElementById('page_view').innerHTML;
        routerView.innerHTML += document.getElementById('page_part_footer').innerHTML;
        let page = new Page({pagename:'view',showNum:this.showNum});
        document.getElementById('tab_button_view').classList.add('is-active')
        if (this.viewId !== null && this.viewContent === null) {
          page.fetchViewData(`https://spreadsheets.google.com/feeds/cells/${this.viewId}/1/public/full?alt=json`)
          page.renderPage(this.viewContent);
        } else {
          page.renderPage(this.viewContent);
        }
        break;
      case '#/send/': case '#/send':
        routerView.innerHTML = document.getElementById('page_part_tab').innerHTML;
        routerView.innerHTML += document.getElementById('page_send').innerHTML;
        routerView.innerHTML += document.getElementById('page_part_footer').innerHTML;
        document.getElementById('tab_button_send').classList.add('is-active')
        break;
      case '#/settings/': case '#/settings':
        routerView.innerHTML = document.getElementById('page_part_tab').innerHTML;
        routerView.innerHTML += document.getElementById('page_settings').innerHTML;
        routerView.innerHTML += document.getElementById('page_part_footer').innerHTML;
        document.getElementById('tab_button_settings').classList.add('is-active')
        document.getElementById('settings_viewId').value = localStorage.getItem('google-doc-id')
        document.getElementById('settings_showNum').value = localStorage.getItem('show-num')
        break;

      // default page
      case "": case "#/":
        location.hash = this.defaultHash;
        console.log('to default page '+this.defaultHash)
        break;
      default:
        routerView.innerHTML = document.getElementById('page_part_tab').innerHTML;
        routerView.innerHTML += document.getElementById('page_404').innerHTML;
        routerView.innerHTML += document.getElementById('page_part_footer').innerHTML;
        break;
    }
  }
  handleSearchParams(){
    const params = new URLSearchParams(window.location.search)
    this.searchParams = [];
    for (const param of params) {
      let newparam = {}
      newparam.key = param[0]
      newparam.value = param[1]
      this.searchParams.push(newparam)
    }
  }
}
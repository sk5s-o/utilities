"use strict";
window.addEventListener('DOMContentLoaded', load);
window.addEventListener("hashchange", load);

const href = new Href({
  defaultHash: '#/send/',
  viewId: localStorage.getItem('google-doc-id'),
  viewContent: JSON.parse(sessionStorage.getItem('view-content')),
  showNum: localStorage.getItem('show-num')
})

function load(){
  href.routePage();
  href.handleSearchParams();
}
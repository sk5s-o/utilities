"use strict";
class Page{
  constructor(parameters){
    this.pagename = parameters.pagename;
    this.showNum = parameters.showNum;
  }
  async fetchViewData(myurl){
    const response = await fetch(myurl)
    const data = await response.json()
    console.log(data)
    this.tidyUpData(data)
  }
  tidyUpData(data){
    let cells = data.feed.entry
    console.log(cells)
    let wordsNumber = cells.length
    for (let i = 0; i < cells.length; i++){
      let row = cells[i].gs$cell.row
      let col = cells[i].gs$cell.col
      let content = cells[i].gs$cell.$t
      let word = {
        time: '',
        word: '',
        link: '',
        more: ''
      }
      console.log(`row: ${row}, col: ${col} => ${content}`)
      for (let i = 0; i < 4 - 1; i++){
        switch (col){
          case '1':
            wordsList[row-1] = word
            wordsList[row-1].time = content
            break
          case '2':
            wordsList[row-1].word = content
            break
          case '3':
            wordsList[row-1].link = content
            break
          case '4':
            wordsList[row-1].more = content
            break
          default:
            break
        }
      }
    }
    console.log(wordsList)
    wordsList.shift()
    sessionStorage.setItem('view-content',JSON.stringify(wordsList.reverse().slice(0, this.showNum)))
    location.reload()
    // this.renderPage(JSON.parse(sessionStorage.getItem('view-content')))
  }
  renderPage(wordsList){
    view_main = document.getElementById('view_main_content')
    if (wordsList === null) return;
    console.log('render page')
    view_main.innerHTML = '';
    for (let i = 0; i < wordsList.length; i++){
      let div = document.createElement('div')
      let a = document.createElement('a')
      let span = document.createElement('span')
      let p = document.createElement('p')
      p.innerHTML = ''
      if (wordsList[i].more != ''){
        p.innerHTML = wordsList[i].more
      }
      div.id = `word_div_${i}`
      a.href = wordsList[i].link
      a.target = '_blank'
      a.innerText = wordsList[i].word
      a.classList.add('word-link')
      span.innerText = wordsList[i].time
      span.classList.add('submit-time')
      p.classList.add('more-description')
      div.innerHTML += '<hr>'
      div.appendChild(a)
      div.appendChild(span)
      div.appendChild(p)
      view_main.appendChild(div)
    }
    view_main.innerHTML += '<hr>';
  }
}

let wordsList = []
let parameters
let view_main = document.getElementById('view_main_content')
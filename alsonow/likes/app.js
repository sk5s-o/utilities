"use strict";
window.addEventListener('DOMContentLoaded', load);
window.addEventListener("hashchange", load);

const defaultHash = '#/send/'
let pagePath = ''
let configFileLastTime = 1800 //second e.g.1800
const pathAndPageData = {
  'view':{
    pageTitle:'view',
    pageContent:document.getElementById('page_view').innerHTML,
    header:document.getElementById('page_part_tab').innerHTML,
    footer:document.getElementById('page_part_footer').innerHTML,
    toggleClass:[
      {
        elementId:'tab_button_view',
        className:'is-active'
      }
    ]
  },
  'send':{
    pageTitle:'send',
    pageContent:document.getElementById('page_send').innerHTML,
    header:document.getElementById('page_part_tab').innerHTML,
    footer:document.getElementById('page_part_footer').innerHTML,
    toggleClass:[
      {
        elementId:'tab_button_send',
        className:'is-active'
      }
    ]
  },
  'settings':{
    pageTitle:'settings',
    pageContent:document.getElementById('page_settings').innerHTML,
    header:document.getElementById('page_part_tab').innerHTML,
    footer:document.getElementById('page_part_footer').innerHTML,
    toggleClass:[
      {
        elementId:'tab_button_settings',
        className:'is-active'
      }
    ]
  },
  'fresh':{
    pageTitle:'fresh',
    pageContent:document.getElementById('page_fresh').innerHTML,
    header:'',
    footer:document.getElementById('page_part_footer').innerHTML,
  },
  'fourofour':{
    pageContent:document.getElementById('page_404').innerHTML,
    header:document.getElementById('page_part_tab').innerHTML,
    footer:document.getElementById('page_part_footer').innerHTML
  }
}

function load(){
  gistService();
  renderPageService();
}

async function gistService(){
  let gist = new Gist()
  let date = new Date();
  let storage = new Storage()
  if (localStorage.getItem('gist-id') === null){
    location.hash = '#/fresh/'
  }
  if (localStorage.getItem('gist-content') === null && localStorage.getItem('gist-id') !== null){
    let configFileContent = await gist.fetch(storage.get('gist-id'))
    gist.save('gist-content',configFileContent);
    console.log('saved gist settings');
    let nextTime = date.getTime() + (configFileLastTime*1000);
    storage.save('gist-fetch-next-time', nextTime.toString());
    location.reload();
  }else if (parseInt(storage.get('gist-fetch-next-time')) <= date.getTime() && localStorage.getItem('gist-id') !== null){
    let configFileContent = await gist.fetch(storage.get('gist-id'))
    gist.save('gist-content',configFileContent);
    console.log('renew gist settings');
    let nextTime = date.getTime() + (configFileLastTime*1000);
    storage.save('gist-fetch-next-time', nextTime.toString());
    location.reload();
  }
}
function renderPageService(){
  let router = new Router()
  let page = new Page()
  let hash = router.hashToPagePath(location.hash)
  if (hash === undefined) location.hash = defaultHash
  page.clear('router-view')
  let pageData = router.findPageData(hash)
  if (pageData === undefined) {
    let fourofour = router.findPageData(['fourofour'])
    page.addHtmlToView('router-view',fourofour.header)
    page.addHtmlToView('router-view',fourofour.pageContent)
    page.addHtmlToView('router-view',fourofour.footer)
    page.toggleClass(fourofour.toggleClass)
    page.changeDocTitle(fourofour.pageTitle)
    return;
  }
  page.addHtmlToView('router-view',pageData.header)
  page.addHtmlToView('router-view',pageData.pageContent)
  page.addHtmlToView('router-view',pageData.footer)
  page.toggleClass(pageData.toggleClass)
  page.changeDocTitle(pageData.pageTitle)
  let perpage = new Perpage(hash.join('/'))
  perpage.run()
}
function removeAllStorageService(){
  let selection = confirm('Are you sure?')
  if (selection === false) return
  localStorage.clear();
  sessionStorage.clear();
  location.reload();
}
function removeSessionStorageDataService(){
  sessionStorage.clear();
  location.reload();
}

class Router{
  hashToPagePath(hash){
    if (hash === '') return;
    hash = hash.split('/').filter(element => {
      return element !== null && element !== ''&& element !== '#';
    });
    if (hash.length === 0) return
    return hash
  }
  findPageData(path) {
    if (path === undefined) return
    const routerView = document.getElementById("router-view");
    if (!(routerView instanceof HTMLElement)) {
      throw new ReferenceError("No router view element available for rendering");
    }
    let pathString = path.join('/')
    pagePath = pathString
    let pageData = pathAndPageData[pathString]
    if (pageData === undefined) return
    return pageData
  }
}
class Page{
  clear(mainViewId){
    let mainView = document.getElementById(mainViewId)
    mainView.innerHTML = '';
  }
  addHtmlToView(mainViewId,html){
    if (mainViewId === undefined || mainViewId === '') return
    if (html === '' || html === undefined) return
    let mainView = document.getElementById(mainViewId)
    mainView.innerHTML += html
  }
  toggleClass(myarray){
    if (myarray === undefined) return;
    myarray.forEach(item => {
      let element = document.getElementById(item.elementId)
      element.classList.toggle(item.className);
    })
  }
  changeDocTitle(title){
    let defaultTitle = 'sk5-gfo-likes'
    document.title = `${title === undefined ? '' : title+' | '}${defaultTitle}`
  }
}
class Gist{
  constructor(){
    this.gistKey = 'sk5-gfo-likes'
  }
  async fetch(id){
    if (id === undefined || id === '') return
    let url = `https://api.github.com/gists/${id}`;
    const response = await fetch(url,{
      method : "GET"
    })
    let result = await response.json();
    if (result.files['sk5-gfo-likes-config.json'] === undefined) return
    let result_json = JSON.parse(result.files['sk5-gfo-likes-config.json'].content);
    return result_json;
  }
  save(key,object){
    if (key === undefined || key === '' || object === null) return
    let ciphertext = CryptoJS.AES.encrypt(JSON.stringify(object), this.gistKey).toString();
    localStorage.setItem(key,ciphertext);
  }
  getLocalData(itemName){
    if (itemName === '' || itemName === undefined) return
    let bytes  = CryptoJS.AES.decrypt(localStorage.getItem(itemName), this.gistKey);
    let decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptedData;
  }
}
class Storage{
  constructor(){
    this.key = 'localstorage'
  }
  save(key,value){
    if (key === undefined || key === '' || value === undefined || value === '') return
    let ciphertext = CryptoJS.AES.encrypt(value, this.key).toString();
    localStorage.setItem(key,ciphertext)
  }
  get(key){
    if (key === undefined || key === '') return
    var bytes  = CryptoJS.AES.decrypt(localStorage.getItem(key), this.key);
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
  }
}
class Perpage{
  constructor(pagePath){
    switch (pagePath) {
      case 'fresh':
        this.run = this.fresh
        break;
      case 'view':
        this.run = this.view
        break;
      case 'send':
        this.run = this.send
        break;
      default:
        break;
    }
  }
  run(){
    return;
  }
  fresh(){
    let storage = new Storage()
    if (localStorage.getItem('gist-id') !== null) location.hash = defaultHash
    let input_gistId = document.getElementById('input_gistId')
    let button_gistId = document.getElementById('button_gistId')
    button_gistId.addEventListener('click',() => {
      if (input_gistId.value === '') return
      storage.save('gist-id',input_gistId.value)
      location.hash = defaultHash
    })
  }
  async view(){
    let sheet = new Sheet();
    let gist = new Gist();
    let page = new Page();
    let viewConfig = gist.getLocalData('gist-content').view;
    if (viewConfig === undefined || viewConfig === '') return;
    if (viewConfig.id === undefined || viewConfig.id === '') return;
    let data = []
    if (sessionStorage.getItem('view-content') === null || sessionStorage.getItem('view-content') === ''){
      data = await sheet.fetch(`https://spreadsheets.google.com/feeds/cells/${viewConfig.id}/1/public/full?alt=json`)
      sessionStorage.setItem('view-content',JSON.stringify(data))
    } else {
      data = JSON.parse(sessionStorage.getItem('view-content'))
    }
    let tideData = sheet.tidyUp(data);
    let wordsListHtml = sheet.generateHtml(tideData);
    page.clear('view_main_content');
    page.addHtmlToView('view_main_content',wordsListHtml);
  }
  send(){
    let frame = document.getElementById('send_form_iframe')
    let gist = new Gist()
    let config = gist.getLocalData('gist-content').send
    let baseurl = `https://docs.google.com/forms/d/e/${config.id}/viewform`
    let params = ''
    const urlparams = new URLSearchParams(location.search);
    config.fields.forEach((item) => {
      if (urlparams.has(item.name)){
        if (params !== '') params += '&'
        let entry = `entry.${item.entry}`
        let content = urlparams.get(item.name)
        let pattern = item.pattern;
        pattern = pattern.split('!')
        content = pattern[0]+content+pattern[1];
        params += `${entry}=${content}`
      }
    })
    let question = params === '' ? '' : '?';
    let url = baseurl+question+params
    frame.src = url;
  }
}
class Sheet{
  async fetch(url){
    const response = await fetch(url)
    const data = await response.json()
    console.log('fetch google sheet')
    return data
  }
  tidyUp(data){
    let cells = data.feed.entry;
    let wordsList = [];
    for (let i = 0; i < cells.length; i++){
      let row = cells[i].gs$cell.row - 1
      let col = cells[i].gs$cell.col - 1
      let content = cells[i].gs$cell.$t
      if (wordsList[row] === undefined) wordsList[row] = [];
      if (wordsList[row][col] === undefined) wordsList[row][col] = content;
    }
    wordsList.shift(); //row one is title
    return wordsList.reverse(); //new first
  }
  generateHtml(wordsList){
    let gist = new Gist()
    let viewConfig = gist.getLocalData('gist-content').view
    if (viewConfig === undefined || viewConfig === '') return;
    let dotimes = 0;
    if (viewConfig.number === undefined || viewConfig.number === ''){
      return;
    } else if (viewConfig.number > wordsList.length){
      dotimes = wordsList.length;
    } else {
      dotimes = viewConfig.number;
    }
    let html = '';
    for (let i = 0; i < dotimes; i++){
      let div = document.createElement('div')
      let a = document.createElement('a')
      let span = document.createElement('span')
      let p = document.createElement('p')
      p.innerHTML = ''
      if (wordsList[i][3] !== undefined && wordsList[i][3] !== ''){
        p.innerHTML = wordsList[i][3]
      }
      div.id = `word_div_${i}`
      a.href = wordsList[i][2]
      a.target = '_blank'
      a.innerText = wordsList[i][1]
      a.classList.add('word-link','title','is-4')
      span.innerText = wordsList[i][0]
      span.classList.add('submit-time')
      span.style = 'padding-left:20px;font-size:12px'
      p.classList.add('more-description')
      div.innerHTML += '<hr>'
      div.appendChild(a)
      div.appendChild(span)
      div.appendChild(p)
      html += div.innerHTML;
    }
    return html
  }
}